
import json, os,re
from PyPDF2 import PdfFileReader, PdfFileWriter
from openpyxl import Workbook
from datetime import datetime
from my_pdf import PDFXTract
from time import time

class PDFExtract:
    def __init__(self, file_ext):

        self.pages_text, self.readers, self.errors, self.success  =[],[],[], []
        self.enr_typ, self.fname, self.lname, self.dob, self.gender = [],[],[],[],[]
        self.enr_code, self.new_hcp_name, self.new_hcp_add, self.emp_code = [],[],[],[]
        self.new_nhis, self.new_hcp_code, =[],[]

        self.wb = Workbook()
        self.des_file ="Test.xlsx"
        self.act_wb = self.wb.active
        self.act_wb.title ='Police HMO Data'

        st_tit = ['S/N','NHIS No','Firstname','Surname',
                  'Date of birth','Gender','Enrollee Type',
                  'Enrollee Code','Employer Code',
                  'HCP Code', 'HCP Name','HCP Address']

        #set the header cells
        for i in range(len(st_tit)):
            self.act_wb.cell(row=1, column=i+1).value = st_tit[i]
        
        self.pdf_files = self.get_files_by_ext(file_ext)
        for pdf_file in self.pdf_files:
            self.readers.append(PdfFileReader(pdf_file, 'r'))

    #get all pdfs files in the folder
    def get_files_by_ext(self, ext):
        extracted_files= []
        dir_files = os.listdir(os.path.dirname(os.path.realpath(__file__)))
        for file in dir_files:
            for y in range(1, 6):
                if file[-y:]==ext:
                    extracted_files.append(file)    
        return extracted_files
        
    #load pages
    def load_pages(self):
        if len(self.pdf_files)==1:
            #iterate each page self.readers[0].getNumPages()
            for i in range(63, 66):
                pageText = self.readers[0].getPage(i).extractText().split('*0')
                self.extract_all_relevant(pageText, i)
            self.dump_cont()
        else:
            print 'Please provide one file at a time!'

    #MMain manipulation of the collected data
    def extract_all_relevant(self, collection, page_no):
        for text in collection:
            #remove last empty members
            text_collection = text.split('\n')[0:-1]
            if "Issued by NHIS" not in text_collection:
                #a normal text
                #check ['Printed on blah] ['Next page']
                if (len(text_collection)-1)%5 !=0:
                    text_collection = text_collection[0:-2]
            else:
                #[extract hospital data from the headed data]
                hcp_code = text_collection[2]
                hcp_name = text_collection[3].split('-')[0]
                #Check whether the HCP details are longer
                if len(text_collection[3].split('-'))>1:
                    hcp_add = text_collection[3].split('-')[1]
                else:
                    hcp_add = text_collection[3].split('-')[0]
                    #final cleaned data
                text_collection = text_collection[2:]

            #final data cleaning have been done
            if len(text_collection)>4:
                #extract nhis number from first member [*03408527* ADAM]
                nhis = '0{}'.format(text_collection[0])[0:8]
                #[FE/275] seperate the gender and the code, and seperate names with dates
                new_col = self.correct_gen(self.check_date(text_collection[1:]))
                if "E/" not in new_col[len(new_col)-1]:
                        for item in ['XX/XX/XXXX', 'X', 'E/XXX']:
                            new_col.append(item)
                if len(new_col)%6!=0:
                    new_col = self.higher_manip_(new_col)
                countert=0

                for y in range(len(new_col)):
                    if countert%6==0:
                        attr = [self.emp_code,self.fname,self.lname,self.gender,self.enr_typ,self.dob,
                                self.enr_code,self.new_hcp_name,self.new_hcp_add,self.new_nhis,self.new_hcp_code]
                        val = [new_col[y+5],new_col[y+2],new_col[y+1],new_col[y+4],new_col[y][1:],
                                new_col[y+3],new_col[y][0],hcp_name,hcp_add,nhis,hcp_code]
                        for num in range(0, len(attr)):
                            attr[num].append(val[num])
                    countert+=1
            else:
                #length of collection is not >4
                pass
    #split date with part name JOY02/02/1990
    def check_date(self, tex):
        items_found = []
        for text in tex:
            if re.search(r'([a-zA-Z0-9])(\d{2}/\d{2}/\d{2})',text)==None:
                items_found.append(text)
            enr_type_st, sta,end, removed_chars, not_found = [x for x in range(15)], 0,10,[],True
            if len(text)!=10:
                while not_found and sta<len(text):
                    if len(text[sta:end])==10 and text[sta:end][2]=='/' and text[sta:end][5]=='/':
                        try:
                            shek,cur_year=int(text[sta:end][6:10]), datetime.now().year
                            if int(text[sta:end][0]) in enr_type_st[0:4] and shek in range(1900, cur_year+1):
                                items_found.append(''.join(removed_chars))
                                items_found.append(text[sta:end])
                                not_found =False
                            else:
                                break
                        except ValueError:
                            break
                    else:
                        removed_chars.append(text[sta])
                        sta,end=sta+1,end+1
        return items_found
    #split gender with code ME/275
    def correct_gen(self, content):
        items_found=[]
        for item in content:
            if re.search(r'([a-zA-Z])(/\d{3})',item)==None:
                items_found.append(item)
            sta,end, removed_chars, not_found = 0,10,[],True
            while not_found and sta<len(item):
                if len(item[sta:end])==5 and item[sta:end][1]=='/':  
                    items_found.append(''.join(removed_chars))
                    items_found.append(item[sta:end])
                    not_found =False  
                else:
                    removed_chars.append(item[sta])
                    sta,end=sta+1,end+1
        return items_found
    
    def higher_manip_(self, list):
        newList =[]
        counter=0
        for li in list:
            if li[0] in str(range(0, 9)) and '/' not in li:
                tsayi = len(list[counter+1])
                snm = list[counter+1][tsayi/2:]
                fnm = list[counter+1][:tsayi/2]
                #check if there is any name at all
                if '/' not in list[counter+1]:
                    if list[counter+2][2]=='/' and list[counter+2][5]=='/':
                        for l in [list[counter],snm,fnm,list[counter+2],list[counter+3],list[counter+4]]:
                            newList.append(l)
                else:
                    #there is no name at all ['5 Child4', u'XX/XX/XXXX', u'X', u'EXXX']
                    for ll in [list[counter], "Name", list[1][:tsayi/2], list[counter+1], list[counter+2], list[counter+3]]:
                        newList.append(ll)
            counter+=1
        return newList

    #remove empty array member     
    def trim_array(self, content):
        if content[len(content)-1]=='':
            content.remove(content[len(content)-1])
        return content
    #dump to excel and json
    def dump_cont(self):
        s_num=0
        for x in range(len(self.new_nhis)):
            s_num+=1
            details = [
                s_num,
                self.new_nhis[x].encode("utf-8"),
                self.lname[x].encode("utf-8"),
                self.fname[x].encode("utf-8"),
                self.dob[x].encode("utf-8"),
                self.gender[x].encode("utf-8"),
                self.enr_typ[x].encode("utf-8"),
                self.enr_code[x].encode("utf-8"),
                self.emp_code[x].encode("utf-8"),
                self.new_hcp_code[x].encode("utf-8"),
                self.new_hcp_name[x].encode("utf-8"),
                self.new_hcp_add[x].encode("utf-8")
            ]
            self.pages_text.append(details)
        for row in self.pages_text:
            self.act_wb.append(row)
        self.dump_json(self.pages_text, 'testsss.json')
        self.dump_json(self.errors, 'test_errsss.json')

        self.wb.save(filename=self.des_file)
        self.wb.close()

        if len(self.errors)>0:
            print 'Completed with errors... Check Errors.json for details'
        else:
            print 'Successfully done!'

    def dump_json(self, data, fil):
        with open(fil, 'w') as file:
            file.write(json.dumps(data))
            file.close()

pdf_extr =PDFExtract("pdf")
pdf_extr.load_pages()