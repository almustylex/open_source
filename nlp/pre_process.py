"""
1. Sentence is tokenized
2. Stopping words are removed
3. Special Characters are removed
4. Repeated characters are removed
5. Spelling corrections are done
"""
import re,string,json

class Pre_process:

    def token_nize(self, sentence):
        stp_w = json.load(open("stp_words.json", 'r'))
        sentence = sentence.lower().split(" ")
        return [sabo for sabo in sentence if sabo not in stp_w]
    
    def remove_special_chars(self, tokens):

        pattern = re.compile('[{}]'.format(re.escape(string.punctuation))) 
        filtered_tokens = filter(None, [pattern.sub('', token) for token in tokens]) 
        return filtered_tokens

    def remove_repeated_chars(self, tokens):
        repeat_pattern = re.compile(r'(\w*)(\w)\2(\w*)')
        match_substitution = r'\1\2\3'
        def replace(old_word):
            if wordnet.synsets(old_word):
                return old_word
            new_word = repeat_pattern.sub(match_substitution, old_word)
            return replace(new_word) if new_word != old_word else new_word
        correct_tokens = [replace(word) for word in tokens]
        return correct_tokens
        
sen = "Hello my name is Adam Mustapha I am from Jigawa!?"
pre = Pre_process()
tokenized = pre.token_nize(sen)
no_sp_chars = pre.remove_special_chars(tokenized)
print no_sp_chars


        