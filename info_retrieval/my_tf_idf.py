from __future__ import division
import math
import nltk
"""
TFIDF Algorithm implementation
Author Adam Mustapha
"""
class MyTfIDF:
    
    query_docs = []
    merged_doc =[]

    def __init__(self, docs):
        
        for doc in docs:

            dc = nltk.word_tokenize(doc.lower())
            self.query_docs.append(dc)

    #calculate the term_frequency here 
    def term_freq(self, word, doc):
        counter =0
        for item in doc:
            if item==word:
                counter +=1
        tf = counter/float(len(doc))
        return tf

    #compute the document idf
    def idf(self, word):
        docs_qty = len(self.query_docs)
        docs_with_word=0

        for doc in self.query_docs:
            if word in doc:
                docs_with_word +=1
        idf = math.log(docs_qty/float(docs_with_word))
        return idf
    
    #get the union of all th docs to avoid repetition
    def get_union(self):
        for doc in self.query_docs:
            for item in doc:
                if item not in self.merged_doc:
                    self.merged_doc.append(item)
        return self.merged_doc

    #compute the overall tf_idf
    def compute_tf_id(self):
        
        for word in self.get_union():
            print "\n"
            for doc in self.query_docs:
                my_tf = self.term_freq(word, doc)
                id_f = self.idf(word)
                res = my_tf*id_f
                print word,res

    def get_cosine(self):
        cos = math.cos(30)
        #return cos
        print cos
 

tfidf = MyTfIDF(["Searching for the petroleum crisis", "the Nigerian political crisis"])
tfidf.get_cosine()
dd = tfidf.query_docs
print dd
tfidf.compute_tf_id()
